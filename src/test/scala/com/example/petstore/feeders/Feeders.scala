package com.example.petstore.feeders

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import ru.tinkoff.gatling.feeders._
import ru.tinkoff.gatling.utils.RandomDataGenerators

object Feeders {

  // generate random name using regex pattern
  val nameFeeder = RegexFeeder("PET_NAME", "[a-zA-Z]{10}")

  // read csv file and pass values into scenario
  val petCategoriesFeeder = csv("pools/pet_categories.csv").circular

  // generate random string
  val tagsFeeder = RandomStringFeeder("TAG_NAME")

  // generate random digit
  val petIdFeeder = CustomFeeder("PET_ID", RandomDataGenerators.randomDigit(1, 3))

  // generate random date using format
  val shipDateFeeder = RandomDateFeeder("SHIP_DATE", positiveDaysDelta = 5, datePattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
}