package com.example.petstore

import com.example.petstore.conf.Config
import com.example.petstore.scenarios._
import io.gatling.core.Predef._
import ru.tinkoff.gatling.config.SimulationConfig._

class Capacity extends Simulation {

  setUp(
    Scenario.customer.inject(rampConcurrentUsers(0) to Config.customerConcurrency during rampDuration),
    Scenario.seller.inject(rampConcurrentUsers(0) to Config.sellerConcurrency during rampDuration),
  ).throttle(reachRps(intensity.toInt) in rampDuration)
    .protocols(httpProtocol)
    .maxDuration(testDuration)

}
