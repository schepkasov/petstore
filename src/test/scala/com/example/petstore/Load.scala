package com.example.petstore

import com.example.petstore.conf.Config
import io.gatling.core.Predef._
import ru.tinkoff.gatling.config.SimulationConfig._
import com.example.petstore.scenarios._

class Load extends Simulation {

  setUp(
    Scenario.customer.inject(
      rampConcurrentUsers(0) to Config.customerConcurrency during rampDuration,
      constantConcurrentUsers(Config.customerConcurrency) during stageDuration
    ),
    Scenario.seller.inject(
      rampConcurrentUsers(0) to Config.sellerConcurrency during rampDuration,
      constantConcurrentUsers(Config.sellerConcurrency) during stageDuration
    ),
  ).throttle(
    reachRps(intensity.toInt) in rampDuration,
    holdFor(stageDuration)
  )
    .protocols(httpProtocol)
    .assertions(
      global.failedRequests.percent.lt(Config.failedRequestPercentLessThan),
      global.responseTime.percentile3.lte(Config.globalResponseTime95PctLessThan)
    )
    .maxDuration(testDuration)

}
