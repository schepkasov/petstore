package com.example.petstore.conf

import ru.tinkoff.gatling.config.SimulationConfig._

object Config {

  // see resources/simulation.conf

  lazy val orderConversionPercent = getDoubleParam("profile.customer.orderConversionPercent")
  lazy val updatePercent = getDoubleParam("profile.seller.updatePercent")
  lazy val deletePercent = getDoubleParam("profile.seller.deletePercent")

  lazy val customerConcurrency = getIntParam("profile.customer.concurrency")
  lazy val sellerConcurrency = getIntParam("profile.seller.concurrency")


  lazy val apiKey = getStringParam("apiKey")
  lazy val authUsername = getStringParam("authUsername")
  lazy val authPassword = getStringParam("authPassword")

  lazy val failedRequestPercentLessThan = getDoubleParam("assertions.failedRequestPercentLessThan")
  lazy val globalResponseTime95PctLessThan = getIntParam("assertions.globalResponseTime95PctLessThan")

}