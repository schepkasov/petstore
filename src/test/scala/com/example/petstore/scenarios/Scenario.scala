package com.example.petstore.scenarios

import io.gatling.core.Predef._
import com.example.petstore.cases._
import com.example.petstore.feeders._
import com.example.petstore.conf._

object Scenario {

  // get AUTH_TOKEN
  val login =
    exec(_.set("API_KEY", Config.apiKey))
      .exec(Login.postLogin)
      .exec(Login.getAuth)
      .exec(Login.authorize)

  // authorize and loop through customer scenario
  // TBD: could be updated with global credentials storage in order to use open model injection
  val customer = scenario("Customer: view and order pet")
    .exec(login)
    .forever(
      feed(Feeders.shipDateFeeder)
        .exec(Pet.findAvailableAndStoreId)
        .exec(Pet.getById)
        .randomSwitch(
          Config.orderConversionPercent -> exec(Store.placeOrder)
            .exec(Store.getOrderById)
        )
    )

  // authorize and loop through seller scenario
  // TBD: could be updated with global credentials storage in order to use open model injection
  val seller = scenario("Seller: manage pet")
    .exec(login)
    .forever(
      feed(Feeders.nameFeeder)
        .feed(Feeders.petIdFeeder)
        .feed(Feeders.tagsFeeder)
        .feed(Feeders.petCategoriesFeeder)
        .exec(Store.getInventory)
        .exec(Pet.create)
        .exec(Pet.getById)
        .randomSwitch(
          Config.updatePercent -> exec(Pet.update),
          Config.deletePercent -> exec(Pet.delete)
        )
    )
}
