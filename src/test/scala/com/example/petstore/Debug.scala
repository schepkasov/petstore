package com.example.petstore

import io.gatling.http.Predef._
import io.gatling.core.Predef._
import ru.tinkoff.gatling.config.SimulationConfig._
import com.example.petstore.scenarios._

class Debug extends Simulation {

  // for local debug proxyman/fiddler/charlesproxy can be used
  setUp(
    Scenario.customer.inject(atOnceUsers(1)),
    Scenario.seller.inject(atOnceUsers(1)),
  ).protocols(httpProtocol
    // proxy is required on localhost:9090
    .proxy(Proxy("localhost", 9090).httpsPort(9090))
  ).maxDuration(testDuration)

}
