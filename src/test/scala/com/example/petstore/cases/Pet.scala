package com.example.petstore.cases

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object Pet {

  // Get single pet by PET_ID
  val getById = http("GET /v2/pet/{id}")
    .get("/v2/pet/#{PET_ID}")
    .header("api-key", "#{API_KEY}")

  // Get pets list by status
  val findByStatus = http("GET /v2/pet/findByStatus")
    .get("/v2/pet/findByStatus")
    .header("authorization", "Bearer #{AUTH_TOKEN}")

  val findAvailable = findByStatus
    .queryParam("status", "available")

  val findAvailableAndStoreId = findAvailable
    .check(jsonPath("$[*].id").findRandom.saveAs("PET_ID"))

  val findPending = findByStatus
    .queryParam("status", "pending")

  val findSold = findByStatus
    .queryParam("status", "sold")

  val findAll = findByStatus
    .queryParamSeq(
      Seq(
        "status" -> "available",
        "status" -> "pending",
        "status" -> "sold",
      )
    )

  // Add new pet to the store
  val create = http("POST /v2/pet")
    .post("/v2/pet")
    .body(ElFileBody("data/pet/pet.json"))
    .asJson
    .header("authorization", "Bearer #{AUTH_TOKEN}")
    .check(jsonPath("$.id").saveAs("PET_ID"))

  // Update an existing pet
  val update = http("PUT /v2/pet")
    .put("/v2/pet")
    .body(ElFileBody("data/pet/pet.json"))
    .header("authorization", "Bearer #{AUTH_TOKEN}")
    .asJson

  // Delete pet by PET_ID
  val delete = http("DELETE /v2/pet/{id}")
    .delete("/v2/pet/#{PET_ID}")
    .header("api-key", "#{API_KEY}")
    .header("authorization", "Bearer #{AUTH_TOKEN}")

}
