package com.example.petstore.cases

import com.example.petstore.conf.Config
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import ru.tinkoff.gatling.config.SimulationConfig._

object Login {

  val postLogin = http("POST /oauth/login")
    .post("/oauth/login")
    .formParamMap(
      Map(
        "username" -> s"${Config.authUsername}",
        "password" -> s"${Config.authPassword}",
        "login" -> ""
      )
    )
    .header("Content-type", "application/x-www-form-urlencoded")
    .header("Referer", s"$baseUrl/oauth/login.jsp")
    .header("Origin", s"$baseUrl")

  val getAuth = http("GET /oauth/authorize")
    .get("/oauth/authorize")
    .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
    .queryParamMap(
      Map(
        "response_type" -> "token",
        "client_id" -> s"${Config.authUsername}",
        "scope" -> "read:pets"
      )
    )

  val authorize = http("POST /oauth/authorize")
    .post("/oauth/authorize")
    .formParamMap(
      Map(
        "user_oauth_approval" -> "true",
        "scope.read:pets" -> "true",
        "authorize" -> "Authorize"
      )
    )
    .header("Content-type", "application/x-www-form-urlencoded")
    .header("Referer", s"$baseUrl/oauth/login.jsp")
    .header("Origin", s"$baseUrl")
    .check(headerRegex("Location", "access_token=(.*)&token_type").ofType[String].saveAs("AUTH_TOKEN"))

}

