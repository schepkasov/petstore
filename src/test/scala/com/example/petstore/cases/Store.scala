package com.example.petstore.cases

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object Store {

  // Get store inventory count grouped by status
  val getInventory = http("GET /v2/store/inventory")
    .get("/v2/store/inventory")
    .header("api-key", "#{API_KEY}")
    .check(jsonPath("$.available").saveAs("AVAILABLE_COUNT"))

  // Place an order for a pet
  val placeOrder = http("POST /v2/store/order")
    .post("/v2/store/order")
    .body(ElFileBody("data/store/order.json"))
    .asJson
    .check(jsonPath("$.status").is("placed"))
    .check(jsonPath("$.complete").is("true"))
    .check(jsonPath("$.id").saveAs("ORDER_ID"))

  // Get order details by ORDER_ID
  // Note that ORDER_ID should be integer from 1 to 10 (see swagger description)
  val getOrderById = http("GET /v2/store/order/{id}")
    .get("/v2/store/order/#{ORDER_ID}")
    .check(jsonPath("$.status").is("placed"))
    .check(jsonPath("$.complete").is("true"))

}