# Petstore performance test project

[[_TOC_]]

## Performance testing strategy

System under load: https://petstore.swagger.io/#

### Limitations

- No access to application monitoring.
- No info about real application usage or load profile.
- Possible rate limits at the application side.
- Unstable network between load generator and application.
- Some endpoints don`t work well, e.g. /v2/store/order/{id} works only with id in range 1 to 10, possible functional errors, need to investigate.
- 

### Load scenarios

Two scenarios for using the system:
- customer: `login` -> loop(`find available pet` -> `see random available pet details` -> `make an order with some probability`)
- seller: `login` -> loop(`see inventory` -> `create pet` -> `see pet details` -> `update or delete pet with some probability`)

### Tests

- Capacity test: ramp load from 0 to 200 RPS. Determine system capacity.
- Load test: stable load at 80% of capacity.
- 

### Requirements

- Response time percentile 95 less than 2s
- Failed request percent less than 5%

## Project structure

```
src.test.resources - project resources
src.test.scala.com.example.petstore.cases - simple cases
src.test.scala.com.example.petstore.scenarios - common load scenarios assembled from simple cases
src.test.scala.com.example.petstore.feeders - test data generators
src.test.scala.com.example.petstore.conf - test config values
src.test.scala.com.example.petstore - common test configs
```

## Test configuration

### Default configuration

Default configuration params specified at [simulation.conf](src/test/resources/simulation.conf) file.

You can override any param:

- Pass this params to JVM using -DparamName="paramValue" AND -Dconfig.override_with_env_vars=true.
- Set ENV variable with `CONFIG_FORCE_` prefix (see config library [docs](https://github.com/lightbend/config#optional-system-or-env-variable-overrides)).

### Online monitoring

```
Gatling logs:
CONSOLE_LOGGING=ON - turn on console logging
FILE_LOGGING=ON - turn on logging in file "target/gatling/gatling.log"
```

Setup influxdb and grafana (TBD).
```
Gatling metrics in InfluxDB:
GRAPHITE_HOST - influxdb with configured graphite plugin host
GRAPHITE_PORT - see /etc/influxdb/influxdb.conf: bind-address
INFLUX_PREFIX - see /etc/influxdb/influxdb.conf: database
```

## Debug launch

1. Debug test with 1 user, requires proxy on localhost:9090 (use Fiddler, Wireshark, Proxyman, etc).

```
"Gatling/testOnly com.example.petstore.Debug"
```

2. Run test from IDEA with breakpoints

```
com.example.GatlingRunner
```

## Launch test

```
"Gatling/testOnly com.example.petstore.Capacity" - maximum performance test
"Gatling/testOnly com.example.petstore.Load" - stability test
```