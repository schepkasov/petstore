import sbt._

object Dependencies {
  lazy val gatling: Seq[ModuleID] = Seq(
    "io.gatling.highcharts" % "gatling-charts-highcharts",
    "io.gatling"            % "gatling-test-framework",
  ).map(_ % "3.9.3" % Test)

  lazy val gatlingPicatinny: Seq[ModuleID] = Seq("ru.tinkoff" %% "gatling-picatinny" % "0.13.0")
  lazy val janino: Seq[ModuleID]           = Seq("org.codehaus.janino" % "janino" % "3.1.8")
}
